# loadwatcher

Nodejs and npm required.

npm install -g git+https://carlhopf@bitbucket.org/carlhopf/loadwatcher.git

## Command line usage

./app.js <oauth-user> <client-id> <client-secret> <refresh-token> <alert-emails> <cpu-load-limit> <ram-usage-limit> <hdd-usage-limit> <traffic-gb-limit>

## Command line options

- **oauth-user** - Gmail user to send alerts from.
- **client-id** - Oauth client id.
- **client-secret** - Oauth client secret.
- **refresh-token** - Oauth refresh token.
- **alert-emails** - Comma seperated list of emails to send alerts to.
- **cpu-load-limit** - CPU load limit (per core, will be multiplied by cpu count).
- **ram-usage-limit** - RAM usage limit from 0 to 1. Defaults to 0.75.
- **hdd-usage-limit** - HDD usage limit from 0 to 1. Defaults to 0.75.
- **traffic-gb-limit** - Traffic limit in GB.

http://masashi-k.blogspot.hk/2013/06/sending-mail-with-gmail-using-xoauth2.html

'use strict';
var cp = require('child_process');
var config = require('./config');

exports.topMemoryProcesses = function(cb) {
	cp.exec(
		'ps aux --sort \'%mem\' | tail -n 6 | awk {\'print $4,$11,$12,$13\'}',
		function(err, data) {
			var lines = data && data.toString().split(/\n/g);
			cb(null, lines);
		});
};

exports.topCpuProcesses = function(cb) {
	cp.exec(
		'ps aux --sort \'%cpu\' | tail -n 6 | awk {\'print $3,$11,$12,$13\'}',
		function(err, data) {
			var lines = data && data.toString().split(/\n/g);
			cb(null, lines);
		});
};

exports.ramUsage = function(callback) {
	cp.exec('free -m', function(err, data) {
		console.log('free -m output:', data);
		var lines = data.toString().split(/\n/g);

		if (lines.length < 3) {
			callback(null, 'free -m output could not be parsed');
			return;
		}

		for (var i = 0; i < lines.length; i++) {
			lines[ i ] = lines[ i ].split(/\s+/);
		}

		var ramUsed = parseInt(lines[ 2 ][ 2 ], 10) || 1;
		var ramFree = parseInt(lines[ 2 ][ 3 ], 10) || 1;
		var ramUsage = Math.round(ramUsed / (ramUsed + ramFree) * 100) / 100;

		console.log(
			'ram usage ' + ramUsage + ', ' + ramUsed + ' used, ' + ramFree + ' free');

		callback(null, ramUsage);
	});
};

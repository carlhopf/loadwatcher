#!/usr/bin/env node
'use strict';
var os = require('os');
var fs = require('fs');
var path = require('path');
var nodemailer = require('nodemailer');
var cp = require('child_process');
var async = require('async');
var config = require('./config');
var get = require('./get');

var FILE_LAST = path.join(os.tmpdir(), 'loadwatcher-last.json');

var lastJson = fs.existsSync(FILE_LAST) ?
	JSON.parse(fs.readFileSync(FILE_LAST)) : {};

console.log('last notify ts', lastJson);

var canNotify = function(type, delay) {
	var lastTs = lastJson[ type ] || 0;
	return Date.now() > lastTs + (delay) || 0;
};

var saveLast = function(type) {
	lastJson[ type ] = Date.now();
	fs.writeFileSync(FILE_LAST, JSON.stringify(lastJson));
};

var DELAY_NOTIFY_TRAFFIC = 1 * 24 * 60 * 60 * 1000;

// create tokens for gmail access:
// http://masashi-k.blogspot.hk/2013/06/sending-mail-with-gmail-using-xoauth2.html
var OAUTH_USER = process.argv[ 2 ];
var CLIENT_ID = process.argv[ 3 ];
var CLIENT_SECRET = process.argv[ 4 ];
var REFRESH_TOKEN = process.argv[ 5 ];

// comma seperated list
var EMAILS_ALERT = process.argv[ 6 ];

var CPU_LIMIT = process.argv[ 7 ] || 1;
var HDD_LIMIT = process.argv[ 9 ] || 0.75;
var TRAFFIC_GB_LIMIT = parseInt(process.argv[ 10 ], 10) || 100;

if (!OAUTH_USER)
	throw new Error('OAUTH_USER missing');

if (!CLIENT_ID)
	throw new Error('CLIENT_ID missing');

if (!CLIENT_SECRET)
	throw new Error('CLIENT_SECRET missing');

if (!REFRESH_TOKEN)
	throw new Error('REFRESH_TOKEN missing');

if (!EMAILS_ALERT)
	throw new Error('EMAILS_ALERT missing');

// comma seperated list if using multiple emails
EMAILS_ALERT = EMAILS_ALERT.split(',');

if (EMAILS_ALERT.length === 0) {
	throw new Error('EMAILS_ALERT missing');
}

var generator = require('xoauth2').createXOAuth2Generator({
	user: OAUTH_USER,
	clientId: CLIENT_ID,
	clientSecret: CLIENT_SECRET,
	refreshToken: REFRESH_TOKEN
});

var transporter = nodemailer.createTransport({
	service: 'gmail',
	auth: {
		xoauth2: generator
	}
});

async.auto({
	load: function(callback) {
		var cpuLoad15 = Math.round(os.loadavg()[ 2 ] * 100) / 100;
		var cpuLimit = CPU_LIMIT * config.CPU_COUNT;
		console.log('cpu load ' + cpuLoad15 + ', limit ' + cpuLimit);
		callback(null, cpuLoad15 > cpuLimit ? cpuLoad15 : 0);
	},

	// get free RAM (without buffers/caches) on ubuntu
	ram: function(callback) {
		get.ramUsage(function(err, ramUsage) {
			if (!config.RAM_LIMIT) throw new Error('no ram limit');
			return callback(err, ramUsage >= config.RAM_LIMIT ? ramUsage : 0);
		});
	},

	topCpuProcesses: function(cb) {
		get.topCpuProcesses(cb);
	},

	topMemoryProcesses: function(cb) {
		get.topMemoryProcesses(cb);
	},

	disk: function(callback) {
		cp.exec('df -h /', function(err, data) {
			console.log('df output:', data);

			var lines = data.toString().split(/\n/g);
			var hddUsage = 0;

			if (lines.length > 1 && lines[ 1 ].length > 4) {
				hddUsage = (parseInt(lines[ 1 ].split(/\s+/)[ 4 ], 10) || 0) / 100;
			}

			console.log('hdd usage ' + hddUsage + ', limit ' + HDD_LIMIT);
			callback(null, hddUsage > HDD_LIMIT ? hddUsage : 0);
		});
	},

	traffic: function(cb) {
		// total traffic (in and out) parsed from vnstat,
		// ubuntu only
		cp.exec(
			'vnstat -m | grep estimated | awk {\'print $8,$9\'}',
			function(err, stdout) {
				var parts = stdout.split(' ');
				var unit = parts[1];
				var value = parseFloat(parts[0]) || 0;
				value = Math.round(value * 100) / 100;

				// always adjust to GiB
				switch (unit) {
					case 'KiB':
						value /= 1024 * 1024;
						break;
					case 'MiB':
						value /= 1024;
						break;
					case 'TiB':
						value *= 1024;
						break;
				}

				var can = canNotify('traffic', DELAY_NOTIFY_TRAFFIC);

				if (value > TRAFFIC_GB_LIMIT && can) {
					saveLast('traffic');
					cb(null, value);
				} else {
					cb(null, 0);
				}
			});
	}
}, function(err, results) {
	var alerts = [];

	if (results.load) {
		alerts.push(
			'load avg: ' + JSON.stringify(os.loadavg()) +
			' (' + config.CPU_COUNT + ' cores)');
	}

	if (results.ram) {
		alerts.push(
			'RAM usage: ' + Math.round(results.ram * 100) + '%');
	}

	if (results.disk) {
		alerts.push('HDD usage: ' + results.disk);
	}

	if (results.traffic) {
		alerts.push(
			'Traffic estimated: ' + results.traffic + 'gb');
	}

	if (err) {
		alerts.push(err);
	}

	if (alerts.length !== 0 || err) {
		console.log('send alerts', alerts);

		var message =
			os.hostname() +
			'\n\n' +
			alerts.join('\n') +
			'\n\n' +
			'top cpu:\n- ' + results.topCpuProcesses.join('\n- ') +
			'\n\n' +
			'top mem:\n- ' + results.topMemoryProcesses.join('\n- ') +
			'\n\n';

		transporter.sendMail({
			from: OAUTH_USER,
			to: EMAILS_ALERT.join(', '),
			subject: 'loadwatcher ' + os.hostname(),
			text: message,
		}, function(err, info) {
			if (err) {
				console.error('error sending email', err);
			} else {
				console.log('message sent', info.response);
			}
		});
	} else {
		console.log('no alerts to send');
	}
});

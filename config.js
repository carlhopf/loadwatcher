'use strict';
var os = require('os');

module.exports = {
	RAM_LIMIT: process.argv[ 8 ] || 0.75,
	CPU_COUNT: os.cpus().length,
};
